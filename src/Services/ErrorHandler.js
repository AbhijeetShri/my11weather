import React from "react";
import { connect } from "react-redux";
import ActionTypes from "../Actions/ActionTypes";
function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,

    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

class ErrorHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false
    };
  }
  componentDidCatch() {
    this.setState({
      hasError: true
    });
    this.props.dispatch({ type: ActionTypes.TURN_OFF_FLAGS });
  }
  render() {
    if (this.state.hasError) {
      return (
        <div class="ErrorLoader">
          <h3>Some error has occured. Kindly try again.</h3>
          <h4>Suggestion:</h4>
          <h5>Check if city you gave in search was correctly spelled</h5>
          <h5>
            May be there might have been some network issue. Do check later
          </h5>
          <h5>Contact the developer</h5>
        </div>
      );
    }
    return this.props.children;
  }
}

export default connect(mapStateToProps)(ErrorHandler);
