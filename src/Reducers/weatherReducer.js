import ls from "local-storage";
import ActionTypes from "../Actions/ActionTypes";

export function weatherReducer(
  state = {
    history: [],
    favourites: ls.get("favourites") || [],
    showCurrent: true,
    showMoreInfo: false,
    lastSearch: "",
    tempInFahrenhiet: ls.get("tempInFahrenhiet") || false
  },
  action
) {
  switch (action.type) {
    case ActionTypes.SEARCH_DATA_RECIEVED:
      const history = [...state.history];
      if (!history.includes(action.payload)) history.push(action.payload);

      state = {
        history: history,
        favourites: state.favourites,
        showCurrent: true,
        showMoreInfo: false,
        lastSearch: action.payload,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;

    case ActionTypes.ADD_FAVOURITES:
      let favourites = [...state.favourites];
      favourites.push(state.lastSearch);
      ls.set("favourites", favourites);
      state = {
        history: state.history,
        favourites: favourites,
        showCurrent: state.showCurrent,
        showMoreInfo: state.showMoreInfo,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;

    case ActionTypes.SHOW_MORE_INFO:
      state = {
        history: state.history,
        favourites: state.favourites,
        showCurrent: false,
        showMoreInfo: true,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;

    case ActionTypes.CLEAR_HISTORY:
      state = {
        history: [],
        favourites: state.favourites,
        showCurrent: state.showCurrent,
        showMoreInfo: state.showMoreInfo,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;

    case ActionTypes.TOGGLE_TEMPERATURE_SCALE:
      var temp = !(state.tempInFahrenhiet ? true : false);
      ls.set("tempInFahrenhiet", temp);
      state = {
        history: state.history,
        favourites: state.favourites,
        showCurrent: state.showCurrent,
        showMoreInfo: state.showMoreInfo,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: temp
      };

      return state;
    case ActionTypes.RESTORE:
      state = {
        history: state.history,
        favourites: state.favourites,
        showCurrent: true,
        showMoreInfo: false,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;
    case ActionTypes.TURN_OFF_FLAGS:
      state = {
        history: state.history,
        favourites: state.favourites,
        showCurrent: false,
        showMoreInfo: false,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      return state;

    case ActionTypes.REMOVE_FAVOURITES:
      let favourite = [...state.favourites];
      for (var i = 0; i < favourite.length; i++) {
        if (favourite[i] === action.payload) {
          favourite.splice(i, 1);
        }
      }
      console.log(favourite);
      state = {
        history: state.history,
        favourites: favourite,
        showMoreInfo: state.showMoreInfo,
        showCurrent: state.showCurrent,
        lastSearch: state.lastSearch,
        tempInFahrenhiet: state.tempInFahrenhiet
      };
      ls.set("favourites", favourite);
      return state;
    default:
      return state;
  }
}
