import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";

function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,

    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

class Favourites extends React.Component {
  render() {
    return (
      <div style={{ "margin-top": "5%", "font-size": "20px" }}>
        You have {this.props.favourites.length} favourites :
        <div>
          {this.props.favourites.map(item => {
            return (
              <div>
                <div>
                  <NavLink to={`/Favorites/${item}`}>{item}</NavLink>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Favourites);
