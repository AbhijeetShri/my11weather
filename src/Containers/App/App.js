import React from "react";
import "./App.css";
import Search from "../Search/Search";
import MoreInfo from "../DetailedView/MoreInfo";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { weatherReducer } from "../../Reducers/WeatherReducer";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import History from "../History/History";
import Favourites from "../Favorites/Favourites";
import Settings from "../Settings/Settings";
import ErrorHandler from "../../Services/ErrorHandler";

//initialising store
const store = createStore(weatherReducer);

class App extends React.Component {
  /**
   * render method to display the base component
   * contains the navigation menu with the route configuration
   */
  render() {
    return (
      <Provider store={store}>
        <ErrorHandler>
          <div className="App">
            <div className="Logo">
              <img
                src="http://openweathermap.org/img/wn/10d@2x.png"
                alt="my11logo"
                width="10%"
                height="10%"
              ></img>
              <b>My11Weather</b>
            </div>

            <Router>
              <div className="navigation">
                <ul
                  key="0"
                  style={{
                    "list-style": "none"
                  }}
                >
                  <li key="1">
                    <Link to="/WeatherInformation/">
                      <button>Home</button>
                    </Link>
                  </li>
                  <li key="2">
                    <Link to="/History">
                      <button>History</button>
                    </Link>
                  </li>
                  <li key="3">
                    <Link to="/Favourites">
                      <button>Favourites</button>
                    </Link>
                  </li>
                  <li key="4">
                    <Link to="/Settings">
                      <button>Settings</button>
                    </Link>
                  </li>
                </ul>
              </div>

              <Route exact path="/" component={Search} />
              <Route path="/WeatherInformation/:city?" component={Search} />
              <Route path="/Favorites/:city?" component={MoreInfo} />
              <Route path="/MoreInformation/:city?" component={MoreInfo} />
              <Route path="/History" component={History} />
              <Route path="/Favourites" component={Favourites} />
              <Route path="/Settings" component={Settings} />
            </Router>

            <div>
              <h6>
                created with &hearts;<br></br> Developed by Abhijeet Shrivastava
              </h6>
            </div>
          </div>
        </ErrorHandler>
      </Provider>
    );
  }
}

export default App;
