import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import "./Weather.css";
import { Link } from "react-router-dom";
import Popup from "reactjs-popup";
import ActionTypes from "../../Actions/ActionTypes";

/**
 *maps state to props for component so that store data is accessible for component
 * @param {*} state
 */
function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,
    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

/**
 *function responsible to take the response of API fetch and convert it into an object of discrete properties
 * @param {*} body
 */
function parseResponse(body) {
  let weather = null;
  let weatherKeys = Object.values(body);
  let parser = obtainDiscreteKeys(weatherKeys);
  weather = {
    name: parser.name,
    base: parser.base,
    date: parser.date,
    resultCode: parser.resultCode,
    id1: parser.id,
    longitude: parser.coord.lon,
    latitude: parser.coord.lat,
    desc: parser.weatherDesc[0].description,
    icon: parser.weatherDesc[0].icon,
    id: parser.weatherDesc[0].id,
    main: parser.weatherDesc[0].main,
    temperature: parser.main.temp,
    pressure: parser.main.pressure,
    humidity: parser.main.humidity,
    minimumtemp: parser.main.temp_min,
    maximumtemp: parser.main.temp_max,
    country: parser.sys.country,
    sunrise: parser.sys.sunrise,
    sunset: parser.sys.sunset,
    wind: parser.wind.speed,
    degree: parser.wind.deg,
    visibility: parser.visibility,
    clouds: parser.clouds.all
  };
  return weather;
}

/**
 *helper function to break all the arrays of response bodies to discrete keys
 * @param {*} weatherKeys
 */
function obtainDiscreteKeys(weatherKeys) {
  let parser = null;
  try {
    parser = {
      coord: weatherKeys[0],
      weatherDesc: weatherKeys[1],
      base: weatherKeys[2],
      main: weatherKeys[3],
      visibility: weatherKeys[4],
      wind: weatherKeys[5],
      clouds: weatherKeys[6],
      date: weatherKeys[7],
      sys: weatherKeys[8],
      timezone: weatherKeys[9],
      id: weatherKeys[10],
      name: weatherKeys[11],
      resultCode: weatherKeys[12]
    };
  } catch (error) {
    throw new Error("Parser Error");
  }

  return parser;
}

/**
 * class component weatherinfo responsible for showing the basic information of weather of city searched in Search.js component
 */
class WeatherInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      searchResponse: [],
      showError: false
    };
    this.addFavourite = this.addFavourite.bind(this);
    this.showAddFavouritesWidget = this.showAddFavouritesWidget.bind(this);
  }

  /**
   *Lifecycle method
   */
  componentDidMount() {
    const node = ReactDOM.findDOMNode(this.refs.weather);
    this.fetchSearchData(this.props.city);
    node.scrollIntoView();
  }

  /**
   *lifecycle method
   */
  componentWillUnmount() {
    this.props.dispatch({ type: ActionTypes.RESTORE });
  }

  /**
   *lifecycle method
   * @param {*} nextProps
   * @param {*} nextState
   */
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.city !== nextProps.city) {
      this.setState({ loader: true, searchResponse: [] });
      this.fetchSearchData(nextProps.city);
    }
    return true;
  }

  /**
   * function responsible for calling the OpenWeatherMap Api to get the details of the city weather
   * @param {*} term
   */
  fetchSearchData(term) {
    let forecastUrl = `https://community-open-weather-map.p.rapidapi.com/weather?q=${term}`;

    fetch(forecastUrl, {
      method: "GET",
      headers: {
        "RapidAPI-Project": "default-application_3993100",
        "X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
        "x-RapidAPI-Key": "c9404e7f60mshb73df7d6a0d5171p17495djsn6c8cd9286bda"
      }
    })
      .then(data => data.json())
      .then(response => {
        this.setState({
          loader: false,
          searchResponse: parseResponse(response),
          showError: false
        });

        this.props.dispatch({
          type: ActionTypes.SEARCH_DATA_RECIEVED,
          payload: term
        });
      })
      .catch(error => {
        this.setState({
          showError: true
        });
        this.props.dispatch({ type: ActionTypes.TURN_OFF_FLAGS });
      });
  }

  /***
   function responsible to take the Kelvin temperature and return in another scale as saved in settings.js page
   */
  showTemperature = temp => {
    if (this.props.tempInFahrenhiet) {
      temp = Math.round((temp - 273.15) * (9 / 5)) + 32;
      temp = temp + "˚ F";
      return temp;
    }
    temp = Math.round(temp - 273.15) + " ˚ C";
    return temp;
  };

  /**
   * function to dispatch an action letting reducer maintain the favourites section of the application
   */
  addFavourite = () => {
    this.props.dispatch({ type: ActionTypes.ADD_FAVOURITES });
  };

  /**
   * function to dispatch an action letting reducer maintain the new favourites section after removing a city
   */
  removeFavourite = () => {
    console.log("I am being called");
    let city = this.props.city;
    this.props.dispatch({ type: ActionTypes.REMOVE_FAVOURITES, payload: city });
  };

  /**
   * component toggler for detailedView page
   */
  showMoreInfo = () => {
    this.props.dispatch({ type: ActionTypes.SHOW_MORE_INFO, city: null });
  };

  /**
   *Error handler toggler
   */
  showErrorHandler() {
    if (this.state.showError) {
      return (
        <div class="ErrorLoader">
          <h3>Some error has occured. Kindly try again.</h3>
          <h4>Suggestion:</h4>
          <h5>1.) Check if city you gave in search was correctly spelled</h5>
          <h5>
            2.) May be there might have been some network issue. Do check later
          </h5>
          <h5>3.) Contact the developer</h5>
        </div>
      );
    }
  }

  /**
   * widget to handle the add favourite functionality
   */
  showAddFavouritesWidget() {
    if (this.props.favourites.includes(this.props.city)) {
      return (
        <div class="modal-content">
          Remove from Favourite
          <span onClick={this.removeFavourite}>
            <Popup
              trigger={<button className="heart fa fa-heart"></button>}
              modal
              closeOnDocumentClick
            >
              <h3>Added to favorites</h3>
              <br></br>
              <p style={{ "font-size": "15px" }}>Press Esc</p>
            </Popup>
          </span>
        </div>
      );
    } else {
      return (
        <div class="modal-content">
          Add to Favourites
          <span onClick={this.addFavourite}>
            <Popup
              trigger={<button className="heart fa fa-heart-o"></button>}
              modal
              closeOnDocumentClick
            >
              <h3>Removed from favorites</h3>
              <br></br>
              <p style={{ "font-size": "15px" }}>Press Esc</p>
            </Popup>
          </span>
        </div>
      );
    }
  }

  /**
   * render method
   */
  render() {
    let weatherInfoDisplay,
      loaderDisplay,
      flag = null;
    let source =
      "http://openweathermap.org/img/wn/" +
      this.state.searchResponse.icon +
      "@2x.png";
    if (this.state.searchResponse !== null) {
      flag =
        "https://www.countryflags.io/" +
        this.state.searchResponse.country +
        "/flat/64.png";
    }

    if (this.state.loader && this.props.showCurrent) {
      loaderDisplay = <div className="loader"></div>;
    }
    if (this.props.showCurrent) {
      weatherInfoDisplay = (
        <div ref="weather" className="Weather">
          <div>
            {/* <span className={flag}></span> */}
            <img src={flag} alt="country" width="20%" height="20%"></img>
            {this.state.searchResponse.country}
          </div>
          <br></br>
          <div style={{ "font-size": "30px" }}>
            <b>Location</b>: {this.props.city}
          </div>
          <div>
            <img src={source} alt="weather" width="20%" height="20%"></img>
            <div>
              <b style={{ "font-family": "cursive", "font-size": "40px" }}>
                {this.state.searchResponse.desc}
              </b>
            </div>
          </div>
          <div className="row WeatherRow">
            <div className="col-lg-6 ">
              <div>
                <b>Temperature</b>:
                {this.showTemperature(this.state.searchResponse.temperature)}
              </div>
            </div>
            <div className="col-lg-6">
              <div>
                <b>Wind</b>: {this.state.searchResponse.wind} met/sec
              </div>
            </div>
          </div>
          <div className="row WeatherRow">
            <div className="col-lg-6 ">
              <div>
                <b>Latitude</b>: {this.state.searchResponse.latitude} &#176;{" "}
              </div>
            </div>
            <div className="col-lg-6">
              <div>
                <b>Longitude</b>: {this.state.searchResponse.longitude} &#176;
              </div>
            </div>
          </div>
          <div className="row WeatherRow">
            <div className="col-lg-6 ">
              <div>
                <b>Humidity</b>: {this.state.searchResponse.humidity} %
              </div>
            </div>
            <div className="col-lg-6">
              <div>
                <b>Pressure</b>: {this.state.searchResponse.pressure} atmP
              </div>
            </div>
          </div>
          <div>
            {this.showAddFavouritesWidget()}
            <br></br>
            Show More Info
            <Link to={`/MoreInformation/${this.state.searchResponse.name}`}>
              <i
                onClick={this.showMoreInfo}
                className="icon fa fa-info-circle"
              ></i>
            </Link>
          </div>
        </div>
      );
    }
    let moreInfoPage = null;
    if (this.props.showMoreInfo) {
      moreInfoPage = (
        <Link
          to={`/fav/${this.state.searchResponse.name}`}
          city={this.props.lastSearch}
        ></Link>
      );
    }

    return (
      <div>
        {loaderDisplay}
        {weatherInfoDisplay}
        {moreInfoPage}
        {this.showErrorHandler()}
      </div>
    );
  }
}

export default connect(mapStateToProps)(WeatherInfo);
