import React from "react";
import { connect } from "react-redux";
import WeatherInfo from "../BasicInformation/WeatherInfo";

function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,
    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

/**
 *
 */
class Search extends React.Component {
  /**
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    let state = {
      input: "",
      showWeather: false,
      city: "",
      cities: []
    };
    this.state = this.shouldShowWeatherOnLoad(state);
    this.handleChange = this.handleChange.bind(this);
    this.displayLocationInfo = this.displayLocationInfo.bind(this);
  }

  /**
   * function to detect the current location of the browser
   */
  detectLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.displayLocationInfo);
    } else {
      console.log("location not able to get detect");
    }
  };

  /**
   * method responsible for making the API fetch for finding all the nearest cities and assigning it to the state variable
   * @param {*} position
   */
  displayLocationInfo(position) {
    const lng = position.coords.longitude;
    const lat = position.coords.latitude;
    console.log(`longitude : ${lng} | latutude : ${lat}`);
    let cities = [];
    fetch(
      "https://geocodeapi.p.rapidapi.com/GetNearestCities?latitude=" +
        lat +
        "&longitude=" +
        lng +
        "&range=0",
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "geocodeapi.p.rapidapi.com",
          "x-rapidapi-key": "c9404e7f60mshb73df7d6a0d5171p17495djsn6c8cd9286bda"
        }
      }
    )
      .then(data => data.json())
      .then(response => {
        for (let i = 0; i < response.length; i++) {
          cities.push(response[i].City);
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({
      cities: cities
    });
    return cities;
  }

  /**
   *component life cycle method
   * @param {*} state
   */
  shouldShowWeatherOnLoad(state) {
    if (this.props.match.params && this.props.match.params.city) {
      state.showWeather = true;
      state.city = this.props.match.params.city;
    }
    return state;
  }
  /**
   * button click function to change the state of the component
   */
  searchAPI = () => {
    this.setState({
      showWeather: true,
      city: this.state.input
    });
  };

  /**
   * input handler to assign the input's text to state variable
   */
  handleChange = event => {
    this.setState({ input: event.target.value });
  };

  /**
   * widget function to show the basic information through WeatherInfo.js component
   */
  renderWeatherWidget() {
    if (this.state.showWeather) {
      return (
        <div>
          <div id="modal"></div>
          <WeatherInfo
            city={
              this.state.city.charAt(0).toUpperCase() + this.state.city.slice(1)
            }
          ></WeatherInfo>
        </div>
      );
    } else {
      return null;
    }
  }

  /**
   * small form to handle the search event
   */
  renderSearchBox() {
    return (
      <form
        onSubmit={e => {
          e.preventDefault();
          this.setState({
            input: ""
          });
        }}
      >
        <input
          value={this.state.input}
          onChange={this.handleChange}
          type="text"
          placeholder="Enter the city"
          list="cities"
          onFocus={this.detectLocation}
        ></input>
        <datalist className="detector" id="cities">
          {this.state.cities.map(item => {
            return <option style={{ color: "red" }} value={item}></option>;
          })}
        </datalist>
        <button className="btn" type="submit" onClick={this.searchAPI}>
          Search
        </button>
      </form>
    );
  }

  /**
   * render method
   */
  render() {
    return (
      <div>
        <br></br>
        {this.renderSearchBox()}
        {this.renderWeatherWidget()}
      </div>
    );
  }
}

export default connect(mapStateToProps)(Search);
