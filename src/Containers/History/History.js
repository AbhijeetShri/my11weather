import React from "react";
import { connect } from "react-redux";

import { NavLink } from "react-router-dom";

function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,
    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

class History extends React.Component {
  render() {
    let historyDisplay = null;
    if (this.props.history.length) {
      historyDisplay = (
        <div style={{ "margin-top": "5%", "font-size": "20px" }}>
          You have searched {this.props.history.length} cities for checking
          weather conditions:
          {this.props.history.map(item => {
            return (
              <div>
                <NavLink to={`/WeatherInformation/${item}`}>{item}</NavLink>
              </div>
            );
          })}
        </div>
      );
    } else {
      historyDisplay = (
        <div style={{ "margin-top": "5%", "font-size": "20px" }}>
          No cities searched till now...
        </div>
      );
    }

    return <div>{historyDisplay}</div>;
  }
}
export default connect(mapStateToProps)(History);
