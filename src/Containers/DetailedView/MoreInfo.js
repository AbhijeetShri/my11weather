import React from "react";
import { connect } from "react-redux";
import "./MoreInfo.css";
import { withRouter } from "react-router-dom";

/**
 * mapstatetoprops gets connected with connect HOC to map store properties as props to the current component
 * @param {*} state
 */
function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,
    tempInFahrenhiet: state.tempInFahrenhiet
  };
}

/**
 * class component MoreInfo responsible for showing Detailed View Page
 */
class MoreInfo extends React.Component {
  constructor(props) {
    super(props);
    let state = {
      loader: true,
      moreInfoResponse: [],
      city: ""
    };
    this.state = this.shouldShowWeatherOnLoad(state);
  }

  /**
   *function to decide which city's data should be loaded because one request can come from WeatherInfo.js and second can come from Favourites.js
   * @param {*} state
   */
  shouldShowWeatherOnLoad(state) {
    if (this.props.match.params && this.props.match.params.city) {
      state.city = this.props.match.params.city;
    } else {
      state.city = this.props.city;
    }
    return state;
  }

  /**
   * temperature calculation function to return temperature from Kelvin to celsius or fahrenhiet as per the store condition
   */
  showTemperature = temp => {
    if (this.props.tempInFahrenhiet) {
      temp = Math.round((temp - 273.15) * (9 / 5)) + 32;
      temp = temp + "˚ F";
      return temp;
    }
    temp = Math.round(temp - 273.15) + " ˚ C";
    return temp;
  };

  /***
   * fetching the content when the component has rendered and showed the loader part
   */
  componentDidMount() {
    let forecastUrl =
      "https://community-open-weather-map.p.rapidapi.com/forecast?q=" +
      this.state.city;
    fetch(forecastUrl, {
      method: "GET",
      headers: {
        "RapidAPI-Project": "default-application_3993100",
        "X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
        "x-RapidAPI-Key": "c9404e7f60mshb73df7d6a0d5171p17495djsn6c8cd9286bda"
      }
    })
      .then(data => data.json())
      .then(response => {
        this.setState({
          loader: false,
          moreInfoResponse: response
        });
      });
  }

  /**
   * render method
   */
  render() {
    let display = null;
    if (this.state.loader) {
      display = (
        <div>
          <div className="loader">></div>{" "}
        </div>
      );
    } else {
      display = (
        <div style={{ "margin-top": "5%" }}>
          <h3>
            A detailed view page of {this.state.moreInfoResponse.city.name}
          </h3>

          <h4>Population: {this.state.moreInfoResponse.city.population}</h4>
          <p>
            Weather forecast of {this.state.moreInfoResponse.city.name} for next
            5 days of every 3 hours
          </p>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Date/Time</th>
                <th>Temperature</th>
                <th>Pressure</th>
                <th>Sea Level</th>
                <th>Humidity</th>
                <th>Description</th>
              </tr>
            </thead>
            <tbody>
              {this.state.moreInfoResponse.list.map(item => {
                let source =
                  "http://openweathermap.org/img/wn/" +
                  item.weather[0].icon +
                  "@2x.png";
                console.log(source);
                return (
                  <tr>
                    <th>{item.dt_txt}</th>
                    <th>{this.showTemperature(item.main.temp)}</th>
                    <th>{item.main.pressure} atmP</th>
                    <th>{Math.round(item.main.sea_level, 1)} m</th>
                    <th>{item.main.humidity} %</th>
                    <th>
                      <img
                        src={source}
                        alt="description"
                        width="60px"
                        height="60px"
                      ></img>
                      {item.weather[0].description}
                    </th>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
    }

    return <div>{display}</div>;
  }
}

export default withRouter(connect(mapStateToProps)(MoreInfo));
