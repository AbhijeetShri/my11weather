import React from "react";
import { connect } from "react-redux";
import ActionTypes from "../../Actions/ActionTypes";

function mapStateToProps(state) {
  return {
    history: state.history,
    favourites: state.favourites,
    showCurrent: state.showCurrent,
    showMoreInfo: state.showMoreInfo,
    lastSearch: state.lastSearch,

    tempInFahrenhiet: state.tempInFahrenhiet
  };
}
class Settings extends React.Component {
  /***
   * life cycle method to provide css to show the current state of temperature metric
   */
  componentDidMount() {
    var isfahr = this.props.tempInFahrenhiet;
    if (isfahr) {
      var style_fahr = document.getElementById("fahr");
      style_fahr.className = "btn btn-success";
      var style_cel = document.getElementById("celsius");
      style_cel.className = "btn";
    } else {
      style_fahr = document.getElementById("celsius");
      style_fahr.className = "btn btn-success";
      style_cel = document.getElementById("fahr");
      style_cel.className = "btn";
    }
  }

  /**
   * life cycle method to handle the css if the temperature settings are changed
   */
  componentDidUpdate() {
    var isfahr = this.props.tempInFahrenhiet;
    if (isfahr) {
      var style_fahr = document.getElementById("fahr");
      style_fahr.className = "btn btn-success";
      var style_cel = document.getElementById("celsius");
      style_cel.className = "btn";
    } else {
      style_fahr = document.getElementById("celsius");
      style_fahr.className = "btn btn-success";
      style_cel = document.getElementById("fahr");
      style_cel.className = "btn";
    }
  }

  /**
   * function to toggle between the two temperature scales
   */
  metricToggler = () => {
    this.props.dispatch({ type: ActionTypes.TOGGLE_TEMPERATURE_SCALE });
  };

  /**
   * clear history function
   */
  clearHistory = () => {
    this.props.dispatch({ type: ActionTypes.CLEAR_HISTORY });
  };

  /**
   * render method
   */
  render() {
    return (
      <div>
        This page allows you to change the settings of how you see the
        application
        <br></br>
        <br></br>
        <div>
          <div>
            <p style={{ "font-weight": "200" }}>Show Temperature : </p>
            <button
              id="celsius"
              style={{ margin: "10px" }}
              className="btn"
              onClick={this.metricToggler}
            >
              Celsius{" "}
            </button>
            <button id="fahr" className="btn " onClick={this.metricToggler}>
              Fahrenhiet
            </button>
          </div>
          <br></br>
          <br></br>
          <div>
            <button className="btn btn-primary" onClick={this.clearHistory}>
              Clear previous history{" "}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Settings);
